# How to Use

```
python OBmin.py \
--steps 500 \
-i input.pdb \
-o output.pdb \
--ff UFF \
--algorithm cg \
--costraints 0,10 \
```


# Installation

The dependencies can be installed by using the following two methods:

##A) Through Anaconda Cloud:

Anaconda can be downloaded directly from its web page:
https://www.continuum.io/downloads


After the installation of Anaconda, it is required to install the below dependencies:
```
conda install -c openbabel openbabel=2.4.1
```




#######################################################################################################


##B) Manual installation:

For manual installation, the users are required to install the following dependencies to run
OBmin tool. We expect the users to have python 3 installed (the OBmin tool was tested with
python 3.6).
```
openbabel 2.4.1 or greater
	-libpng-1.6.28 or greater
	-pixman-0.34.0 or greater
	-cairo-1.14.8 or greater
	-zlib-1.2.11 or greater
	-swig-3.0.12 or greater
	-Eigen 3.3.3 or greater

```
