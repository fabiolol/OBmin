from optparse import OptionParser
import sys
from openbabel import openbabel
from openbabel import pybel



def optP():
	"""Parse command line options.

	"""

	usage="python3 %prog " \
		  "--steps 500 " \
		  "-i input.pdb " \
		  "-o output.pdb " \
		  "--ff UFF " \
		  "--algorithm cg " \
		  "--input_format mol " \
		  "--add_hydrogen yes " \
		  "--constraints [0,10] " \


	description="Energy Minimization."
	version="\n%prog Version 1\n\nRequires Python 3, Open Babel Python" \
			"bindings."

	optParser = OptionParser(usage=usage,
							 version=version,
							 description=description)


	optParser.set_defaults(ff="UFF", 
						   steps="500",
						   output="openbabel.inMOL.pdb",
						   algorithm="cg")
	


	optParser.add_option('--ff', type='str',
						dest='ff',
						help="Forcefield: UFF, MMFF94 , MMFF94s, Ghemical, GAFF"
						" [default: %default]")



	optParser.add_option('--steps', type='str',
						 dest='steps',
						 help="How many steps?"
						 " [default: %default]")


	optParser.add_option('-i', type='str',
						 dest='input',
						 help="Iutput structure file"
						 " [default: %default]")


	optParser.add_option('-o', type='str',
						 dest='output',
						 help="Output structure file"
						 " [default: %default]")



	optParser.add_option('--algorithm', type='str',
						 dest='algorithm',
						 help="Algorithm: steep (Steepest Descent) or cg (Conjugate Gradients)"
						 " [default: %default]")


	optParser.add_option('--constraints', type='str',
						 dest='constraints',
						 help="Constraints: fixing atom coordinates. Specify a range(e.g., 0,10"
						 " [default: %default]")

	optParser.add_option('--input_format', type='str',
						 dest='format',
						 help="Formats: mol, pdb"
						 " [default: %default]")
	

	optParser.add_option('--add_hydrogen', type='str',
						 dest='add_hydrogen',
						 help="yes or no"
						 " [default: %default]")




	options, args = optParser.parse_args()

	if options.input is None:
		s = "Error: PDB input file is required."
		print(s)
		sys.exit(2)

	return options, args



def main():

	mol = next(pybel.readfile(str(options.format), options.input)).OBMol

	if options.add_hydrogen == "no":
		print("No hydrogens added")

	else:
		print("Adding Hydrogens")
		mol.AddHydrogens()


	constraints = openbabel.OBFFConstraints()

	if options.constraints is None:
		print("No Contraints")

	else:

		print ("Range of Atom to constraints: " + str(options.constraints))
		fix = options.constraints.strip("[]").split(",")
		for i in range(int(fix[0]),int(fix[1])):
			print ("Adding position constraint on atom number "+str(i) )
			constraints.AddAtomConstraint(i)
	

	conv = openbabel.OBConversion()
	print("Forcefield: "+str(options.ff))
	print ("Number of Steps: " + str(options.steps))

	ob_ff = openbabel.OBForceField.FindForceField(options.ff)
	ob_ff.Setup(mol, constraints)
	
	if options.algorithm == "cg":
		print("Algorithm: Conjugate Gradients")
		print ("OpenBabel Minimization...")
		ob_ff.ConjugateGradients(int(options.steps))
	
	else:
		print("Algorithm: Steepest Descent")
		print ("OpenBabel Minimization...")
		ob_ff.SteepestDescent(int(options.steps))


	ob_ff.GetCoordinates(mol)
			
	# Output functionalized NP structure (pdb)
	conv.WriteFile(mol,options.output)
	print ("Done.")



if __name__ == "__main__":
	options, args = optP()
	main()




if __name__ == "__main__":
	options, args = optP()
	main()

